image:
  name: docker.wimes.fr/pibrli/ci-image-java-8-sbt-1.5.3:latest
  username: $NEXUS_USER
  password: $NEXUS_PASSWORD
clone:
  depth: full
options:
  docker: true
definitions:
  steps:
    - step: &last-unstable-version-badge
        name: Unstable version badge
        image: python:3
        script:
          - source set_env.sh
          - curl https://img.shields.io/static/v1\?label\=Last%20unstable%20version\&message\=$VERSION\&color\=orange > last_unstable_version.svg
          - pipe: atlassian/bitbucket-upload-file:0.1.2
            variables:
              BITBUCKET_USERNAME: $BITBUCKET_USER
              BITBUCKET_APP_PASSWORD: $BITBUCKET_PASSWORD
              FILENAME: 'last_unstable_version.svg'
          - curl https://img.shields.io/static/v1\?label\=Last%20unstable%20version%20rwimes\&message\=$VERSION\&color\=orange > last_unstable_version_rwimes.svg
          - pipe: atlassian/bitbucket-upload-file:0.1.2
            variables:
              BITBUCKET_USERNAME: $BITBUCKET_USER
              BITBUCKET_APP_PASSWORD: $BITBUCKET_PASSWORD
              FILENAME: 'last_unstable_version_rwimes.svg'
    - step: &last-stable-version-badge
        name: Stable version badge
        image: python:3
        script:
          - source set_env.sh
          - curl https://img.shields.io/static/v1\?label\=Last%20stable%20version\&message\=$VERSION\&color\=green > last_stable_version.svg
          - pipe: atlassian/bitbucket-upload-file:0.1.2
            variables:
              BITBUCKET_USERNAME: $BITBUCKET_USER
              BITBUCKET_APP_PASSWORD: $BITBUCKET_PASSWORD
              FILENAME: 'last_stable_version.svg'
          - curl https://img.shields.io/static/v1\?label\=Last%20stable%20version%20rwimes\&message\=$VERSION\&color\=green > last_stable_version_rwimes.svg
          - pipe: atlassian/bitbucket-upload-file:0.1.2
            variables:
              BITBUCKET_USERNAME: $BITBUCKET_USER
              BITBUCKET_APP_PASSWORD: $BITBUCKET_PASSWORD
              FILENAME: 'last_stable_version_rwimes.svg'
    - step: &get-version-develop
        name: Get version
        image: mcr.microsoft.com/dotnet/sdk:5.0
        script:
          - dotnet tool install GitVersion.Tool --tool-path ./ --version 5.6.6
          - git fetch origin master:master
          - MAJORMINORPATCH=$(./dotnet-gitversion /showVariable MajorMinorPatch)
          - echo "export VERSION=$MAJORMINORPATCH-SNAPSHOT" >> set_env.sh
        artifacts:
          - set_env.sh
    - step: &get-version-feature
        name: Get version
        image: mcr.microsoft.com/dotnet/sdk:5.0
        script:
          - dotnet tool install GitVersion.Tool --tool-path ./ --version 5.6.6
          - git fetch origin master:master
          - git fetch origin develop:develop
          - MAJORMINORPATCH=$(./dotnet-gitversion /showVariable MajorMinorPatch)
          - export FEATURE=$(echo $BITBUCKET_BRANCH | cut -d'/' -f2)
          - echo "export VERSION=$MAJORMINORPATCH-$FEATURE-SNAPSHOT" >> set_env.sh
          - echo "export FEATURE_BRANCH_NAME=$BITBUCKET_BRANCH" >> set_env.sh
        artifacts:
          - set_env.sh
    - step: &get-version-master
        name: Get version
        image: mcr.microsoft.com/dotnet/sdk:5.0
        script:
          - dotnet tool install GitVersion.Tool --tool-path ./ --version 5.6.6
          - MAJORMINORPATCH=$(./dotnet-gitversion /showVariable MajorMinorPatch)
          - echo "export VERSION=$MAJORMINORPATCH" >> set_env.sh
        artifacts:
          - set_env.sh
    - step: &tag-version
        name: Tag version
        script:
          - source set_env.sh
          - git pull
          - git tag $VERSION
          - git push --tags
    - step: &update-version-to-description
        name: Update the version to description
        script:
          - source set_env.sh
          - "export OLD_VERSION=$(cat DESCRIPTION | grep Version: |  sed  's/Version: //g')"
          - "sed -i -e 's/.*Version: '$OLD_VERSION'*/Version: '$VERSION'/' DESCRIPTION"
          - git add DESCRIPTION
          - git commit -m "[skip ci] change version for DESCRIPTION to $VERSION" || true
          - git push
    - step: &publish-R-package-snapshot
        name: Publish R Package
        script:
          - git pull
          - "export VERSION=$(cat DESCRIPTION | grep Version: |  sed  's/Version: //g')"
          - "export PACKAGE=$(cat DESCRIPTION | grep Package: |  sed  's/Package: //g')"
          - mkdir -p /tmp/$PACKAGE
          - cp -r * /tmp/$PACKAGE
          - rm /tmp/$PACKAGE/bitbucket-pipelines.yml          
          - cd /tmp
          - tar -czvf ${PACKAGE}_${VERSION}.tar.gz $PACKAGE
          - ls /tmp
          - curl --upload-file /tmp/${PACKAGE}_${VERSION}.tar.gz https://nexus.wimes.fr/repository/r-snapshot/src/contrib/${PACKAGE}_${VERSION}.tar.gz
          - curl --upload-file /tmp/${PACKAGE}_${VERSION}.tar.gz https://nexus.wimes.fr/repository/r-snapshot/r-hosted/src/contrib/${PACKAGE}_${VERSION}.tar.gz
    - step: &publish-R-package-release
        name: Publish R Package
        script:
          - git pull
          - "export VERSION=$(cat DESCRIPTION | grep Version: |  sed  's/Version: //g')"
          - "export PACKAGE=$(cat DESCRIPTION | grep Package: |  sed  's/Package: //g')"
          - mkdir -p /tmp/$PACKAGE
          - cp -r * /tmp/$PACKAGE
          - rm /tmp/$PACKAGE/bitbucket-pipelines.yml          
          - cd /tmp
          - tar -czvf ${PACKAGE}_${VERSION}.tar.gz $PACKAGE
          - ls /tmp
          - curl -v --user 'intelij:Xp7tem3nNb4FxhYs' --upload-file /tmp/${PACKAGE}_${VERSION}.tar.gz https://nexus.wimes.fr/repository/r-release/src/contrib/${PACKAGE}_${VERSION}.tar.gz
          - curl -v --user 'intelij:Xp7tem3nNb4FxhYs' --upload-file /tmp/${PACKAGE}_${VERSION}.tar.gz https://nexus.wimes.fr/repository/r-release/r-hosted/src/contrib/${PACKAGE}_${VERSION}.tar.gz
    - step: &merge-master-in-develop
        name: Merge master in develop
        script:
          - git pull
          - git fetch origin develop:develop
          - git checkout develop
          - git merge --no-ff -m "[skip ci] merge master in develop" master
          - git push
    - step: &merge-feature-in-develop
        name: Merge feature/* in develop
        script:
          - git pull
          - git fetch origin develop:develop
          - git checkout develop
          - git merge --no-ff -m "merge $BITBUCKET_BRANCH in develop" $BITBUCKET_BRANCH
          - git push
    - step: &merge-develop-in-master-minor
        name: Merge develop in master
        script:
          - git fetch origin master:master
          - git checkout master
          - "git merge --no-ff -m '+semver: minor : merge develop in master' develop"
          - git push
    - step: &merge-develop-in-master-major
        name: Merge develop in master
        script:
          - git fetch origin master:master
          - git checkout master
          - "git merge --no-ff -m '+semver: major : merge develop in master' develop"
          - git push
    - step: &deploy-aqua-notitia-test-snapshot
        name: aqua-notitia-test
        deployment: aqua-notitia-test
        clone:
          enabled: false
        script:
          - source set_env.sh
          - pipe: atlassian/trigger-pipeline:5.0.0
            variables:
              BITBUCKET_USERNAME: $BITBUCKET_USER
              BITBUCKET_APP_PASSWORD: $BITBUCKET_PASSWORD
              REPOSITORY: 'deployment-aquanotitia-dev'
              REF_NAME: 'master'
              CUSTOM_PIPELINE_NAME: 'updateRWimesVersion'
              PIPELINE_VARIABLES: >
                [{
                  "key": "VERSION",
                  "value": "$VERSION"
                },{
                   "key": "RWIMES_NEXUS_PACKAGE",
                   "value": "r-snapshot"
                 }]
              WAIT: 'true'
          - wget https://webhooks.brl.fr/hooks/aqua_notitia_test
    - step: &deploy-aqua-notitia-test
        name: aqua-notitia-test
        deployment: aqua-notitia-test
        clone:
          enabled: false
        script:
          - source set_env.sh
          - pipe: atlassian/trigger-pipeline:5.0.0
            variables:
              BITBUCKET_USERNAME: $BITBUCKET_USER
              BITBUCKET_APP_PASSWORD: $BITBUCKET_PASSWORD
              REPOSITORY: 'deployment-aquanotitia-dev'
              REF_NAME: 'master'
              CUSTOM_PIPELINE_NAME: 'updateRWimesVersion'
              PIPELINE_VARIABLES: >
                [{
                  "key": "VERSION",
                  "value": "$VERSION"
                },{
                   "key": "RWIMES_NEXUS_PACKAGE",
                   "value": "r-release"
                 }]
              WAIT: 'true'
          - wget https://webhooks.brl.fr/hooks/aqua_notitia_test

    - step: &deploy-aqua-notitia
        name: aqua-notitia
        deployment: aqua-notitia
        clone:
          enabled: false
        script:
          - source set_env.sh
          - pipe: atlassian/trigger-pipeline:5.0.0
            variables:
              BITBUCKET_USERNAME: $BITBUCKET_USER
              BITBUCKET_APP_PASSWORD: $BITBUCKET_PASSWORD
              REPOSITORY: 'deployment-aquanotitia-prod'
              REF_NAME: 'master'
              CUSTOM_PIPELINE_NAME: 'updateRWimesVersion'
              PIPELINE_VARIABLES: >
                [{
                  "key": "VERSION",
                  "value": "$VERSION"
                },{
                   "key": "RWIMES_NEXUS_PACKAGE",
                   "value": "r-release"
                 }]
              WAIT: 'true'
          - wget https://webhooks.brl.fr/hooks/aqua_notitia
    - step: &deploy-wimes-demo-test-snapshot
        name: wimes-demo-test
        deployment: wimes-demo-test
        clone:
          enabled: false
        script:
          - source set_env.sh
          - pipe: atlassian/trigger-pipeline:5.0.0
            variables:
              BITBUCKET_USERNAME: $BITBUCKET_USER
              BITBUCKET_APP_PASSWORD: $BITBUCKET_PASSWORD
              REPOSITORY: 'deployment-picto-dev'
              REF_NAME: 'master'
              CUSTOM_PIPELINE_NAME: 'updateRWimesVersion'
              PIPELINE_VARIABLES: >
                [{
                  "key": "VERSION",
                  "value": "$VERSION"
                },{
                   "key": "RWIMES_NEXUS_PACKAGE",
                   "value": "r-snapshot"
                 }]
              WAIT: 'true'
          - wget https://webhooks.brl.fr/hooks/wimes_demo_test
    - step: &deploy-wimes-demo-test
        name: wimes-demo-test
        deployment: wimes-demo-test
        clone:
          enabled: false
        script:
          - source set_env.sh
          - pipe: atlassian/trigger-pipeline:5.0.0
            variables:
              BITBUCKET_USERNAME: $BITBUCKET_USER
              BITBUCKET_APP_PASSWORD: $BITBUCKET_PASSWORD
              REPOSITORY: 'deployment-picto-dev'
              REF_NAME: 'master'
              CUSTOM_PIPELINE_NAME: 'updateRWimesVersion'
              PIPELINE_VARIABLES: >
                [{
                  "key": "VERSION",
                  "value": "$VERSION"
                },{
                   "key": "RWIMES_NEXUS_PACKAGE",
                   "value": "r-release"
                 }]
              WAIT: 'true'
          - wget https://webhooks.brl.fr/hooks/wimes_demo_test

    - step: &deploy-wimes-picto
        name: wimes-picto
        deployment: wimes-picto
        clone:
          enabled: false
        script:
          - source set_env.sh
          - pipe: atlassian/trigger-pipeline:5.0.0
            variables:
              BITBUCKET_USERNAME: $BITBUCKET_USER
              BITBUCKET_APP_PASSWORD: $BITBUCKET_PASSWORD
              REPOSITORY: 'deployment-picto-prod'
              REF_NAME: 'master'
              CUSTOM_PIPELINE_NAME: 'updateRWimesVersion'
              PIPELINE_VARIABLES: >
                [{
                  "key": "VERSION",
                  "value": "$VERSION"
                },{
                   "key": "RWIMES_NEXUS_PACKAGE",
                   "value": "r-release"
                 }]
              WAIT: 'true'
          - wget https://webhooks.brl.fr/hooks/wimes_picto
pipelines:
  custom:
    createMinorRelease:
      - step: *merge-develop-in-master-minor
    createMajorRelease:
      - step: *merge-develop-in-master-major

  branches:
    develop:
      - step: *get-version-develop
      - step: *update-version-to-description
      - step: *publish-R-package-snapshot
      - step: *last-unstable-version-badge
      - parallel:
        - step:
            <<: *deploy-aqua-notitia-test-snapshot
            trigger: manual
        - step:
            <<: *deploy-wimes-demo-test-snapshot
            trigger: manual
    master:
      - step: *get-version-master
      - step: *update-version-to-description
      - step: *tag-version
      - step: *publish-R-package-release
      - step: *last-stable-version-badge
      - parallel:
        - step: *merge-master-in-develop
        - step:
            <<: *deploy-aqua-notitia-test
            trigger: manual
        - step:
            <<: *deploy-aqua-notitia
            trigger: manual
        - step:
            <<: *deploy-wimes-demo-test
            trigger: manual
        - step:
            <<: *deploy-wimes-picto
            trigger: manual
    feature/*:
      - step: *get-version-feature
      - step: *update-version-to-description
      - step: *publish-R-package-snapshot
      - step: *last-unstable-version-badge
      - parallel:
        - step:
            <<: *merge-feature-in-develop
            trigger: manual
        - step:
            <<: *deploy-aqua-notitia-test-snapshot
            trigger: manual
        - step:
            <<: *deploy-wimes-demo-test-snapshot
            trigger: manual