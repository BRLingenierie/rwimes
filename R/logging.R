# use roxygen2:
# https://cran.r-project.org/web/packages/roxygen2/vignettes/rd.html
# run roxygen2::roxygenise() to generate documentation


# Format and log a message in the console
.logWithDate <- function(level, log) {
  cat(.formatLog(level, log), sep = "\n")
}

# Format a message for logging
.formatLog <- function(level, log) {
  return(paste("[", Sys.time(), "] [", level,"] ", log, sep = ""))
}

# Log in the console a message with info level
.logWimes <- function(level, log) {
  checkGlobalVariables()
  checkLibraries()

  dateTime <- as.character(as.POSIXct(Sys.time(), format="%Y-%m-%d %H:%M:%S"))

  request <- paste(getWimesHost(),
                   "/scripting/api/v1/",
                   get("WIMES_KEY", envir = .GlobalEnv),
                   "/log",
                   sep = "")

  response <- POST(request, body = list(log = log,
                                        level = level,
                                        executionId = get("WIMES_EXECUTION_ID", envir = .GlobalEnv),
                                        dateTime = dateTime))
  #checkStatus(response)

  json <- content(response, as="text", encoding="UTF-8")
  result <- jsonlite::fromJSON(txt=json)

  return(TRUE)
}

#' Log in the console a message with info level and save it in the WIMES application
#'
#' @export
#' @param log String - Log message
logInfo <- function(log) {
  if (exists("WIMES_EXECUTION_ID") && !is.null(get("WIMES_EXECUTION_ID", envir = .GlobalEnv))) {
    .logWimes("INFO", log)
  }

  .logWithDate("INFO", log)
}

#' Log in the console a message with warning level and save it in the WIMES application
#'
#' @export
#' @param log String - Log message
logWarning <- function(log) {
  if (exists("WIMES_EXECUTION_ID") && !is.null(get("WIMES_EXECUTION_ID", envir = .GlobalEnv))) {
    .logWimes("WARNING", log)
  }

  .logWithDate("WARNING", log)
}

#' Log in the console a message with error level and save it in the WIMES application
#'
#' @export
#' @param log String - Log message
logError <- function(log) {
  if (exists("WIMES_EXECUTION_ID") && !is.null(get("WIMES_EXECUTION_ID", envir = .GlobalEnv))) {
    .logWimes("ERROR", log)
  }

  .logWithDate("ERROR", log)
}

# Log in the console the progression percentage
#' @export
updateProgression <- function(percentage, message) {
  checkGlobalVariables()
  checkLibraries()

  .logWithDate("INFO", paste(percentage, "% - ",message, sep=""))

  if(exists("WIMES_EXECUTION_ID") && !is.null(get("WIMES_EXECUTION_ID", envir = .GlobalEnv))) {
    dateTime <- as.character(as.POSIXct(Sys.time(), format="%Y-%m-%d %H:%M:%S"))

    request <- paste(getWimesHost(),
                     "/scripting/api/v1/",
                     get("WIMES_KEY", envir = .GlobalEnv),
                     "/progression",
                     sep = "")

    response <- POST(request, body = list(message = message,
                                          percentage = percentage,
                                          executionId = get("WIMES_EXECUTION_ID", envir = .GlobalEnv),
                                          dateTime = dateTime))
    checkStatus(response)

    json <- content(response, as="text", encoding="UTF-8")
    result <- jsonlite::fromJSON(txt=json)
  }

  return(TRUE)
}
