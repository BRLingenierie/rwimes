# use roxygen2:
# https://cran.r-project.org/web/packages/roxygen2/vignettes/rd.html
# run roxygen2::roxygenise() to generate documentation

#' Delete an observation value (QuantValue, CorrectableQuantValue, QualValue, CorrectableQualValue, GridValue)
#'
#' @export
#' @param timeSeriesCode String - Time series code
#' @param observationDateTime POSIXct - Observation date time
#' @return Result
#' @examples
#' deleteObservationValue("MF_GRID_P_1H", as.POSIXct("2018-02-12 23:45:00"))
deleteObservationValue <- function(timeSeriesCode, observationDateTime) {
  checkLibraries()
  checkGlobalVariables()

  request <- paste(getWimesHost(),
                   "/coredb/api/v1/",
                   get("WIMES_KEY", envir = .GlobalEnv),
                   "/values/",
                   timeSeriesCode,
                   "/",
                   format(observationDateTime, '%Y-%m-%dT%H:%M:%SZ', tz="UTC"),
                   sep = "")

  response <- DELETE(request)

  checkStatus(response)
  json <- content(response, as="text", encoding="UTF-8")
  result <- jsonlite::fromJSON(txt=json)
  return(result)
}

#' Delete a list of observation values (QuantValue, CorrectableQuantValue, QualValue, CorrectableQualValue, GridValue)
#'
#' @export
#' @param timeSeriesCode String - Time series code
#' @param startDateTime POSIXct - Start date time
#' @param endDateTime POSIXct - End date time
#' @return Result
#' @examples
#' deleteObservationValues("MF_GRID_P_1H", as.POSIXct("2018-02-12 23:45:00"), as.POSIXct("2020-02-12 23:45:00"))
deleteObservationValues <- function(timeSeriesCode, startDateTime, endDateTime) {
  checkLibraries()
  checkGlobalVariables()

  request <- paste(getWimesHost(),
                   "/coredb/api/v1/",
                   get("WIMES_KEY", envir = .GlobalEnv),
                   "/values/",
                   timeSeriesCode,
                   "/",
                   format(startDateTime, '%Y-%m-%dT%H:%M:%SZ', tz="UTC"),
                   "/",
                   format(endDateTime, '%Y-%m-%dT%H:%M:%SZ', tz="UTC"),
                   sep = "")

  response <- DELETE(request)

  checkStatus(response)
  json <- content(response, as="text", encoding="UTF-8")
  result <- jsonlite::fromJSON(txt=json)
  return(result)
}
