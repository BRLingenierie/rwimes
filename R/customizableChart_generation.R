# use roxygen2:
# https://cran.r-project.org/web/packages/roxygen2/vignettes/rd.html
# run roxygen2::roxygenise() to generate documentation

#################################################
#'
#' editable graph
#' @return graph
#'
customizableGraph_xDate = function(chartConfigParam, isMaestreau) {

  # line and point
  lineWidth = 0.5
  pointSize = 2
  const = 0

  # ts series
  timeSeriesParam = chartConfigParam$series
  if (length(timeSeriesParam) == 0) {
    if (isMaestreau) {
      errorMsg = "Absence de capteur"
    }else {
      errorMsg = "Aucune série trouvée dans la configuration"
    }
    logError(errorMsg)
    stop("e1")
  }

  # times
  startDate = as.POSIXct(chartConfigParam$min / 1000, origin = "1970-01-01", timezone = "UTC")
  endDate = as.POSIXct(chartConfigParam$max / 1000, origin = "1970-01-01", timezone = "UTC")

  # xAxis
  xAxisParam = chartConfigParam$xAxis

  # x limit
  xMin = as.Date(startDate)
  xMax = as.Date(endDate)

  # yAxis
  yAxisParam = chartConfigParam$yAxis

  # calculate yMax and yMin
  yMinCal = c(NA, NA)
  yMaxCal = c(NA, NA)
  for (i in seq(along = timeSeriesParam$code)) {
    timeSerieParam = timeSeriesParam[i,]
    tsValues = getObservedValues(timeSerieParam$code, startDate, endDate, silent = FALSE)
    if (length(tsValues) > 0) {
      if (timeSerieParam$yAxis == yAxisParam$id[2]) {
        yMinCal[2] = min(yMinCal[2], tsValues$value, na.rm = TRUE)
        yMaxCal[2] = max(yMaxCal[2], tsValues$value, na.rm = TRUE)
      }else {
        yMinCal[1] = min(yMinCal[1], tsValues$value, na.rm = TRUE)
        yMaxCal[1] = max(yMaxCal[1], tsValues$value, na.rm = TRUE)
      }
    }
  }
  # y limit
  yMin = as.numeric(yAxisParam$min)
  yMax = as.numeric(yAxisParam$max)
  # consider calculated value if not defined
  yl = yLimit(yMin, yMax, yMinCal, yMaxCal)
  yMin = yl$yMin
  yMax = yl$yMax
  # plot nothing when out of limit
  if (!is.na(yMin[i]) & !is.na(yMaxCal[i] & is.na(yMax[i]) & is.na(yMinCal[i]))) {
    if (yMin[i] > yMaxCal[i] | yMax[i] < yMinCal[i]) {
      if (i == 1) {
        errorMsg = "les données sont hors limites de l'axe principale"
      } else {
        errorMsg = "les données sont hors limites de l'axe secondaire"
      }
      logError(errorMsg)
      graph = ggplot()
      return(graph)
      stop("e1")
    }
  }
  ####
  secondAxis = FALSE
  graphColumn = FALSE
  graphCols = NULL
  graphNames = NULL
  graphShape = NULL
  graphLineStyle = NULL
  trendLineNames = NULL
  scaleFactor = 1
  k = 1
  if (!is.na(yMax[2])) {
    scaleFactor = (as.numeric(yMax[1]) - as.numeric(yMin[1])) / (as.numeric(yMax[2]) - as.numeric(yMin[2]))
  }

  graph = ggplot()
  # plot sensor value
  for (i in seq(along = timeSeriesParam$code)) {
    timeSerieParam = timeSeriesParam[i,]
    graphName = timeSerieParam$name
    graphCols = c(graphCols, timeSerieParam$color)
    graphNames = c(graphNames, graphName)

    # data
    #-------------
    tsValues = getObservedValues(timeSerieParam$code, startDate, endDate, silent = FALSE)
    if (length(tsValues) > 0) {
      # for secondary axis
      const = max(yMin[1], 0)
      if (timeSerieParam$yAxis == yAxisParam$id[2]) {
        secondAxis = TRUE
        tsValues$value = (tsValues$value - yMin[2]) * scaleFactor + const
      }
      gData = data.frame(date = as.Date(tsValues$observationDateTime),
                         value = tsValues$value)

      # add line / point plot
      #-------------
      if (is.na(timeSerieParam$customType) || is.null(timeSerieParam$customType)) {
        timeSerieParam$customType = "line"
      }
      if (timeSerieParam$customType == "line" | timeSerieParam$customType == "marker") {
        graph = graph +
          geom_line(data = gData,
                    aes_string(x = "date",
                               y = "value",
                               color = shQuote(graphName),
                               linetype = shQuote(graphName)),
                    size = lineWidth) +
          geom_point(data = gData,
                     aes_string(x = "date",
                                y = "value",
                                color = shQuote(graphName),
                                shape = shQuote(graphName)),
                     size = pointSize)
      }

      # line style
      if (is.null(timeSerieParam$dashStyle)) timeSerieParam$dashStyle = "Solid"
      if (timeSerieParam$customType == "line") {
        graphLineStyle = c(graphLineStyle, lineStyleCorrespondance(timeSerieParam$dashStyle))
      }else {
        graphLineStyle = c(graphLineStyle, "blank")
      }

      # point style
      #-------------
      if (isTRUE(timeSerieParam$marker$enabled)) {
        graphShape = c(graphShape, pointSymbolCorrespondance(timeSerieParam$marker$symbol))

      } else {
        graphShape = c(graphShape, NA)
      }

      # add bar plot
      #-------------
      if (timeSerieParam$customType == "column") {
        graphColumn = TRUE
        # graphShape = c(graphShape, NA)
        graph = graph + geom_bar(data = gData,
                                 aes_string(x = "date",
                                            y = "value",
                                            fill = shQuote(graphName)),
                                 stat = "identity")
      }

      # trend lines
      #-------------
      if (isTRUE(timeSerieParam$regression)) {
        formulaReg = timeSerieParam$regressionSettings$type
        graphShape = c(graphShape, NA)
        graphLineStyle = c(graphLineStyle, "solid")
        graphCols = c(graphCols, timeSerieParam$regressionSettings$color)

        # add trend line
        tl = trendLine(graph, gData, formulaReg, graphNames, lineWidth)
        graph = tl$graph
        graphNames = tl$graphNames
      }
      #-------------
    }else {
      graphLineStyle = c(graphLineStyle, "blank")
      graphShape = c(graphShape, NA)
    }
  }

  # threshold lines
  #-------------
  # add x line
  threshold_x = xAxisParam$plotLines[[1]]
  if (length(threshold_x$color) > 0) {
    vlines = xLines(graph,
                    threshold_x,
                    yMin,
                    lineWidth,
                    graphLineStyle,
                    graphShape,
                    graphNames,
                    graphCols,
                    chartConfigParam$chartType)
    graph = vlines$graph
    graphLineStyle = vlines$graphLineStyle
    graphShape = vlines$graphShape
    graphNames = vlines$graphNames
    graphCols = vlines$graphCols
  }
  # add y line
  for (i in 1:2) {
    threshold_y = yAxisParam$plotLines[[i]]
    # for secondary axis
    if (i == 2) {
      threshold_y$value = as.numeric(threshold_y$value) * scaleFactor + const
    }
    if (length(threshold_y$id) > 0) {
      hlines = yLines(graph,
                      threshold_y,
                      xMin,
                      lineWidth,
                      graphLineStyle,
                      graphShape,
                      graphNames,
                      graphCols)
      graph = hlines$graph
      graphLineStyle = hlines$graphLineStyle
      graphShape = hlines$graphShape
      graphNames = hlines$graphNames
      graphCols = hlines$graphCols
    }
  }

  # set legend
  numberOfRow = numberOfRowCalculation(graphNames)
  names(graphCols) = graphNames
  if (!is.null(graphShape)) {
    names(graphShape) = graphNames
    names(graphLineStyle) = graphNames
    graph = legendConfiguration(graph, graphLineStyle, graphShape, graphCols, numberOfRow, graphColumn)
  }

  # label & title
  #-------------
  logInfo("secondAxis")
  logInfo(secondAxis)
  graph = axisAndGraphConfiguration(graph,
                                    chartConfigParam,
                                    xMin,
                                    xMax,
                                    yMin,
                                    yMax,
                                    xAxisParam,
                                    yAxisParam,
                                    const,
                                    scaleFactor,
                                    secondAxis)
  return(graph)
}


#################################################
#'
#' editable graph
#' @return graph
#'
customizableGraph_xYearly = function(chartConfigParam, isMaestreau) {
  library(lubridate)

  # line and point
  lineWidth = 0.5
  pointSize = 2

  # ts series
  timeSeriesParam = chartConfigParam$series
  if (length(timeSeriesParam) == 0) {
    if (isMaestreau) {
      errorMsg = "Absence de capteur"
    }else {
      errorMsg = "Aucune série trouvée dans la configuration"
    }
    logError(errorMsg)
    stop("e1")
  }

  # x axis
  xAxisParam = chartConfigParam$xAxis

  # x limit
  xMin = as.Date("01/01/1972", "%d/%m/%Y")
  xMax = as.Date("31/12/1972", "%d/%m/%Y")

  # yAxis
  yAxisParam = chartConfigParam$yAxis

  # graph plotting
  graphCols = timeSeriesParam$color
  graphNames = timeSeriesParam$name
  graphShape = NULL
  graphLineStyle = NULL
  yMinCal = NULL
  yMaxCal = NULL

  graph = ggplot()
  for (i in seq(along = timeSeriesParam$code)) {
    timeSerieParam = timeSeriesParam[i,]
    graphName = timeSerieParam$name

    # times
    startDate = as.POSIXct(strptime(paste0("01/01/", graphName), "%d/%m/%Y"))
    endDate = as.POSIXct(strptime(paste0("31/12/", graphName), "%d/%m/%Y"))

    # data
    #-------------
    tsValues = getObservedValues(timeSerieParam$mainCodeTs, startDate, endDate, silent = FALSE)
    if (length(tsValues) > 0) {
      yMinCal = min(yMinCal, tsValues$value, na.rm = TRUE)
      yMaxCal = max(yMaxCal, tsValues$value, na.rm = TRUE)
      # change year to 1972 (year fictive for plotting)
      year(tsValues$observationDateTime) = 1972
      gData = data.frame(date = as.Date(tsValues$observationDateTime),
                         value = tsValues$value)

      # line / point plot
      #-------------
      if (is.na(timeSerieParam$customType) || is.null(timeSerieParam$customType)) {
        timeSerieParam$customType = "line"
      }
      if (timeSerieParam$customType == "line" | timeSerieParam$customType == "Point") {
        graph = graph +
          geom_line(data = gData,
                    aes_string(x = "date",
                               y = "value",
                               color = shQuote(graphName),
                               linetype = shQuote(graphName)),
                    size = lineWidth) +
          geom_point(data = gData,
                     aes_string(x = "date",
                                y = "value",
                                color = shQuote(graphName),
                                shape = shQuote(graphName)),
                     size = pointSize)
      }

      # line style
      if (is.null(timeSerieParam$dashStyle)) timeSerieParam$dashStyle = "Solid"
      if (timeSerieParam$customType == "line") {
        graphLineStyle = c(graphLineStyle, lineStyleCorrespondance(timeSerieParam$dashStyle))
      }else {
        graphLineStyle = c(graphLineStyle, "blank")
      }

      # point style
      #-------------
      if (isTRUE(timeSerieParam$marker$enabled)) {
        graphShape = c(graphShape, pointSymbolCorrespondance(timeSerieParam$marker$symbol))

      } else {
        graphShape = c(graphShape, NA)
      }

      # trend lines
      #-------------
      if (isTRUE(timeSerieParam$regression)) {
        formulaReg = timeSerieParam$regressionSettings$type
        graphShape = c(graphShape, NA)
        graphLineStyle = c(graphLineStyle, "solid")
        graphCols = c(graphCols, timeSerieParam$regressionSettings$color)

        # add trend line
        tl = trendLine(graph, gData, formulaReg, graphNames, lineWidth)
        graph = tl$graph
        graphNames = tl$graphNames
      }

    }else {
      graphLineStyle = c(graphLineStyle, "blank")
      graphShape = c(graphShape, NA)
    }
  }
  # threshold lines
  #-------------
  # x axis
  threshold_x = xAxisParam$plotLines[[1]]
  if (length(threshold_x$color) > 0) {
    threshold_x$value = as.POSIXct(threshold_x$value[i] / 1000, origin = "1970-01-01", timezone = "UTC")
    vlines = xLines(graph,
                    threshold_x,
                    yMin,
                    lineWidth,
                    graphLineStyle,
                    graphShape,
                    graphNames,
                    graphCols,
                    chartConfigParam$chartType)
    graph = vlines$graph
    graphLineStyle = vlines$graphLineStyle
    graphShape = vlines$graphShape
    graphNames = vlines$graphNames
    graphCols = vlines$graphCols
  }
  # y axis
  threshold_y = yAxisParam$plotLines[[1]]
  if (length(threshold_y$id) > 0) {
    hlines = yLines(graph,
                    threshold_y,
                    xMin,
                    lineWidth,
                    graphLineStyle,
                    graphShape,
                    graphNames,
                    graphCols)
    graph = hlines$graph
    graphLineStyle = hlines$graphLineStyle
    graphShape = hlines$graphShape
    graphNames = hlines$graphNames
    graphCols = hlines$graphCols
  }

  # set legend
  numberOfRow = numberOfRowCalculation(graphNames)
  names(graphCols) = graphNames
  if (!is.null(graphShape)) {
    names(graphShape) = graphNames
    names(graphLineStyle) = graphNames
    graph = legendConfiguration(graph, graphLineStyle, graphShape, graphCols, numberOfRow)
  }

  # y limit
  yMin = as.numeric(yAxisParam$min)[1]
  yMax = as.numeric(yAxisParam$max)[1]
  # consider calculated value if not defined
  yl = yLimit(yMin, yMax, yMinCal, yMaxCal)
  yMin = yl$yMin
  yMax = yl$yMax
  # plot nothing when out of limit
  if (!is.na(yMin[i]) & !is.na(yMaxCal[i] & is.na(yMax[i]) & is.na(yMinCal[i]))) {
    if (yMin[i] > yMaxCal[i] | yMax[i] < yMinCal[i]) {
      if (i == 1) {
        errorMsg = "les données sont hors limites de l'axe principale"
      } else {
        errorMsg = "les données sont hors limites de l'axe secondaire"
      }
      logError(errorMsg)
      graph = ggplot()
      return(graph)
      stop("e1")
    }
  }
  # label & title
  #-------------
  graph = axisAndGraphConfiguration(graph,
                                    chartConfigParam,
                                    xMin,
                                    xMax,
                                    yMin,
                                    yMax,
                                    xAxisParam,
                                    yAxisParam)
  return(graph)
}

#################################################
#'
#' editable graph
#' @return graph
#'
customizableGraph_xValue = function(chartConfigParam, isMaestreau) {

  # line and point
  lineWidth = 0.5
  pointSize = 2

  # ts series
  timeSeriesParam = chartConfigParam$series
  if (length(timeSeriesParam) == 0) {
    if (isMaestreau) {
      errorMsg = "Absence de capteur"
    }else {
      errorMsg = "Aucune série trouvée dans la configuration"
    }
    logError(errorMsg)
    stop("e1")
  }

  # x axis param
  xAxisTSConfig = chartConfigParam$xAxisTSConfig
  xAxisParam = chartConfigParam$xAxis

  # y axis param
  yAxisParam = chartConfigParam$yAxis

  # times
  startDate = as.POSIXct(xAxisTSConfig$minDate / 1000, origin = "1970-01-01", timezone = "UTC")
  endDate = as.POSIXct(xAxisTSConfig$maxDate / 1000, origin = "1970-01-01", timezone = "UTC")

  # x ts value
  xTscode = xAxisTSConfig$xTSCode
  xTsValues = getObservedValues(xTscode, startDate, endDate, silent = FALSE)
  if (length(xTsValues) == 0) {
    errorMsg = "Pas de donnée en axe x"
    logError(errorMsg)
    graph = ggplot()
    stop("e1")
  }

  # graph plotting
  graphCols = timeSeriesParam$color
  graphNames = timeSeriesParam$name
  if (length(graphCols) < length(timeSeriesParam$name)) {
    errorMsg = "Manque des couleurs des courbes"
    logError(errorMsg)
    graph = ggplot()
    stop("e1")
  }
  graphShape = NULL
  graphLineStyle = NULL

  # y and xy data
  #-------------
  xMinCal = NULL
  xMaxCal = NULL
  yMinCal = NULL
  yMaxCal = NULL
  for (i in seq(along = xAxisTSConfig$yTSCode)) {
    timeSerieParam = timeSeriesParam[i,]
    graphName = timeSerieParam$name

    yTscode = xAxisTSConfig$yTSCode[i]
    yTsValues = getObservedValues(yTscode, startDate, endDate, silent = FALSE)
    if (length(yTsValues) == 0) {
      errorMsg = "Pas de donnée en axe y"
      logError(errorMsg)
      graph = ggplot()
      stop("e1")
    }

    gData = NULL
    # xy data
    for (j in seq(along = xTsValues$value)) {
      iRow = which(yTsValues$observationDateTime == xTsValues$observationDateTime[j])
      if (length(iRow) > 0) {
        gData = rbind(gData, data.frame(xValue = xTsValues$value[j],
                                        yValue = yTsValues$value[iRow]))
      }
    }

    if (!is.null(gData)) {
      logInfo("gData is not null")
      if (length(gData) == 0) {
        errorMsg = "Pas de donnée en axe x et y"
        logError(errorMsg)
        graph = ggplot()
        return(graph)
        stop("e1")
      }
    }else {
      logInfo("gData is null")
    }

    # calculate min & max of x axis and y axis
    xMinCal = min(xMinCal, gData$xValue, na.rm = TRUE)
    xMaxCal = max(xMaxCal, gData$xValue, na.rm = TRUE)
    yMinCal = min(yMinCal, gData$yValue, na.rm = TRUE)
    yMaxCal = max(yMaxCal, gData$yValue, na.rm = TRUE)

    # plotting

    graph = ggplot()
    # line / point plot
    #-------------
    if (is.na(timeSerieParam$customType) || is.null(timeSerieParam$customType)) {
      timeSerieParam$customType = "line"
    }
    if (timeSerieParam$customType == "line" | timeSerieParam$customType == "Point") {
      graph = graph +
        geom_line(data = gData,
                  aes_string(x = "xValue",
                             y = "yValue",
                             color = shQuote(graphName),
                             linetype = shQuote(graphName)),
                  size = lineWidth) +
        geom_point(data = gData,
                   aes_string(x = "xValue",
                              y = "yValue",
                              color = shQuote(graphName),
                              shape = shQuote(graphName)),
                   size = pointSize)
    }

    # line style
    if (is.null(timeSerieParam$dashStyle)) timeSerieParam$dashStyle = "Solid"
    if (timeSerieParam$customType == "line") {
      graphLineStyle = c(graphLineStyle, lineStyleCorrespondance(timeSerieParam$dashStyle))
    }else {
      graphLineStyle = c(graphLineStyle, "blank")
    }

    # point style
    #-------------
    if (isTRUE(timeSerieParam$marker$enabled)) {
      graphShape = c(graphShape, pointSymbolCorrespondance(timeSerieParam$marker$symbol))

    } else {
      graphShape = c(graphShape, NA)
    }

    # trend lines
    #-------------
    if (isTRUE(timeSerieParam$regression)) {
      formulaReg = timeSerieParam$regressionSettings$type
      graphShape = c(graphShape, NA)
      graphLineStyle = c(graphLineStyle, "solid")
      graphCols = c(graphCols, timeSerieParam$regressionSettings$color)

      # add trend line
      tl = trendLine(graph, gData, formulaReg, graphNames, lineWidth)
      graph = tl$graph
      graphNames = tl$graphNames
    }
    #-------------
  }

  # threshold lines
  #-------------
  # x axis
  threshold_x = xAxisParam$plotLines[[1]]
  if (length(threshold_x$color) > 0) {
    vlines = xLines(graph,
                    threshold_x,
                    yMinCal,
                    lineWidth,
                    graphLineStyle,
                    graphShape,
                    graphNames,
                    graphCols,
                    chartConfigParam$chartType)
    graph = vlines$graph
    graphLineStyle = vlines$graphLineStyle
    graphShape = vlines$graphShape
    graphNames = vlines$graphNames
    graphCols = vlines$graphCols
  }
  # y axis
  threshold_y = yAxisParam$plotLines[[1]]
  if (length(threshold_y$id) > 0) {
    hlines = yLines(graph,
                    threshold_y,
                    xMinCal,
                    lineWidth,
                    graphLineStyle,
                    graphShape,
                    graphNames,
                    graphCols)
    graph = hlines$graph
    graphLineStyle = hlines$graphLineStyle
    graphShape = hlines$graphShape
    graphNames = hlines$graphNames
    graphCols = hlines$graphCols
  }

  # set legend
  numberOfRow = numberOfRowCalculation(graphNames)
  names(graphCols) = graphNames
  if (!is.null(graphShape)) {
    names(graphShape) = graphNames
    names(graphLineStyle) = graphNames
    graph = legendConfiguration(graph, graphLineStyle, graphShape, graphCols, numberOfRow)
  }


  # x limit
  xMin = xAxisTSConfig$minValue
  xMax = xAxisTSConfig$maxValue

  if (is.null(xMin) || is.na(xMin)) {
    xMin = xMinCal
  }
  if (is.null(xMax) || is.na(xMax)) {
    xMax = xMaxCal
  }
  # plot nothing when out of limit
  if (xMin > xMaxCal | xMax < xMinCal) {
    graph = ggplot()
    stop("e1")
  }

  # y limit
  yMin = as.numeric(yAxisParam$min)[1]
  yMax = as.numeric(yAxisParam$max)[1]
  # consider calculated value if not defined
  yl = yLimit(yMin, yMax, yMinCal, yMaxCal)
  yMin = yl$yMin
  yMax = yl$yMax
  # plot nothing when out of limit
  if (!is.na(yMin[i]) & !is.na(yMaxCal[i] & is.na(yMax[i]) & is.na(yMinCal[i]))) {
    if (yMin[i] > yMaxCal[i] | yMax[i] < yMinCal[i]) {
      if (i == 1) {
        errorMsg = "les données sont hors limites de l'axe principale"
      } else {
        errorMsg = "les données sont hors limites de l'axe secondaire"
      }
      logError(errorMsg)
      graph = ggplot()
      return(graph)
      stop("e1")
    }
  }
  # label & title
  #-------------
  graph = axisAndGraphConfiguration(graph,
                                    chartConfigParam,
                                    xMin,
                                    xMax,
                                    yMin,
                                    yMax,
                                    xAxisParam,
                                    yAxisParam)
  return(graph)
}

