# use roxygen2:
# https://cran.r-project.org/web/packages/roxygen2/vignettes/rd.html
# run roxygen2::roxygenise() to generate documentation

#' Get all sampleMedium
#'
#' @export
#' @return A data frame describing the returned ParameterName objects
#' @examples
#' getSampleMediums()
getSampleMediums <- function() {
  checkLibraries()
  checkGlobalVariables()

  request <- paste(getWimesHost(), "/coredb/api/v1/",
                   get("WIMES_KEY", envir = .GlobalEnv), "/sampleMediums", sep = "")

  return(requestAndParseJson(request))

}

#' Get sampleMedium by code
#'
#' @export
#' @param sampleMediumCode String - Site code
#' @return A data frame describing the returned Site object
#' @examples
#' getSampleMediumByCode("H")
getSampleMediumByCode <- function(sampleMediumCode) {
  checkLibraries()
  checkGlobalVariables()

  request <- paste(getWimesHost(), "/coredb/api/v1/",
                   get("WIMES_KEY", envir = .GlobalEnv), "/sampleMedium/", sampleMediumCode, sep = "")

  return(requestAndParseJson(request))
}
