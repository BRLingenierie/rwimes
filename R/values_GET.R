# use roxygen2:
# https://cran.r-project.org/web/packages/roxygen2/vignettes/rd.html
# run roxygen2::roxygenise() to generate documentation

#' Get the data of an observation time series
#'
#' @export
#' @param timeSeriesCode String - Time series code
#' @param beginDateTime POSIXct - If not null, the method returns only the observations which are after this parameter (optional)
#' @param endDateTime POSIXct - If not null, the method returns only the observations which are before this parameter (optional)
#' @param maxItems Integer - Max number of returned values (optional, default value NULL). If NULL, returns all the available data
#' @param silent Boolean - Active logs if FALSE (optional, default to TRUE)
#' @return A data frame with the returned Value objects
#' @examples
#' getObservedValues("BRL_BRL_FY_Qs")
#' getObservedValues("BRL_BRL_FY_Qs", as.POSIXct("2017-04-13 10:15:30"))
#' getObservedValues("BRL_BRL_FY_Qs", as.POSIXct("2017-04-13 10:15:30"), as.POSIXct("2017-05-13 10:15:30"))
#' getObservedValues("BRL_BRL_FY_Qs", as.POSIXct("2017-04-13 10:15:30"), as.POSIXct("2018-02-14 10:15:30"), 5)
#' getObservedValues("BRL_BRL_FY_Qs", NULL, NULL, 5)
getObservedValues <- function(timeSeriesCode, beginDateTime = NULL, endDateTime = NULL, maxItems = NULL, silent = TRUE) {
  checkLibraries()
  checkGlobalVariables()

  library(dplyr)

  request <- paste(getWimesHost(),
                   "/coredb/api/v3/",
                   get("WIMES_KEY", envir = .GlobalEnv),
                   "/values/",
                   timeSeriesCode,
                   sep = "")

  # optional beginDateTime

  if (!is.null(beginDateTime)) {
    request <- paste(request,
                     "?beginDateTime=",
                     format(beginDateTime, "%Y-%m-%dT%H:%M:%OSZ", tz="UTC"),
                     sep = "")
    nextSeparator <- "&"
  } else {
    nextSeparator <- "?"
  }

  # optional endDateTime

  if (!is.null(endDateTime)) {
    request <- paste(request,
                     nextSeparator,
                     "endDateTime=",
                     format(endDateTime, "%Y-%m-%dT%H:%M:%OSZ", tz="UTC"),
                     sep = "")
  }

  # optional maxItems

  if (is.null(beginDateTime) && is.null(endDateTime)) {
    nextSeparator <- "?"
  } else {
    nextSeparator <- "&"
  }

  if (!is.null(maxItems)) {
    request <- paste(request, nextSeparator, "maxItems=", maxItems, sep="")
  }

  log(request, silent)
  apiResult <- requestAndParseJson(request)

  if (!is.null(apiResult) && !is.null(apiResult$values$observationDateTime)) {
    apiResult$values$observationDateTime <- as.POSIXct(apiResult$values$observationDateTime,
                                                       format = "%Y-%m-%dT%H:%M:%OSZ",
                                                       tz = "UTC")
  }


  if (!is.null(apiResult) && !is.null(apiResult$values$collectionDateTime)) {
    apiResult$values$collectionDateTime <- as.POSIXct(apiResult$values$collectionDateTime,
                                                      format = "%Y-%m-%dT%H:%M:%OSZ",
                                                      tz = "UTC")
  }

  if (is.null(apiResult$prev) && is.null(apiResult$'next')) {
    return(apiResult$values)
  } else if (!is.null(apiResult$prev)) {
    return(getObservedValues_bindAndExecutePrev(apiResult$values,
                                                apiResult$prev,
                                                silent))
  } else {
    return(getObservedValues_bindAndExecuteNext(apiResult$values,
                                                apiResult$'next',
                                                silent))
  }
}

getObservedValues_bindAndExecutePrev <- function(result, requestStr, silent = TRUE) {
  log(requestStr, silent)
  apiResult <- requestAndParseJson(requestStr)

  apiResult$values$observationDateTime <- as.POSIXct(apiResult$values$observationDateTime,
                                                     format = "%Y-%m-%dT%H:%M:%OSZ",
                                                     tz = "UTC")

  apiResult$values$collectionDateTime <- as.POSIXct(apiResult$values$collectionDateTime,
                                                    format = "%Y-%m-%dT%H:%M:%OSZ",
                                                    tz = "UTC")

  if (is.null(apiResult$prev)) {
    return(bind_rows(result, apiResult$values))
  } else {
    return(getObservedValues_bindAndExecutePrev(bind_rows(result, apiResult$values),
                                                apiResult$prev,
                                                silent))
  }
}

getObservedValues_bindAndExecuteNext <- function(result, requestStr, silent = TRUE) {
  log(requestStr, silent)
  apiResult <- requestAndParseJson(requestStr)

  apiResult$values$observationDateTime <- as.POSIXct(apiResult$values$observationDateTime,
                                                     format = "%Y-%m-%dT%H:%M:%0SZ",
                                                     tz = "UTC")

  apiResult$values$collectionDateTime <- as.POSIXct(apiResult$values$collectionDateTime,
                                                    format = "%Y-%m-%dT%H:%M:%0SZ",
                                                    tz = "UTC")

  if (is.null(apiResult$'next')) {
    return(bind_rows(apiResult$values, result))
  } else {
    return(getObservedValues_bindAndExecuteNext(bind_rows(apiResult$values, result),
                                                apiResult$'next',
                                                silent))
  }
}

log <- function(message, silent) {
  if (!silent) {
    cat(message, sep = "\n")
  }
}

#' Download a grid file (GeoTIFF format) identified by its path on the remote WIMES server
#'
#' @export
#' @param filePath String - Path of the wanted GeoTIFF file on the WIMES server
#' @param destfile String - Path where the method downloads the GeoTIFF file (optional, default value temp folder)
#' @return The path to the downloaded GeoTIFF file
#' @examples
#' getGrid("ANTRST_VE/2017/12/11/120000.tiff", tempfile())
getGrid <- function(filePath, destfile = tempfile()) {
  checkLibraries()
  checkGlobalVariables()

  request <- paste(getWimesHost(),
                   "/coredb/api/v1/",
                   get("WIMES_KEY", envir = .GlobalEnv),
                   "/grids/",
                   filePath,
                   sep = "")

  download.file(request, destfile, "auto", quiet = TRUE, mode = "wb");

  return(destfile)
}


#' Get the productions dates after the date given in parameter
#'
#' @export
#' @param timeSeriesCode String - Time series code
#' @param beginDateTime POSIXct - If not null, the method returns only the production dates which are after this parameter (optional)
#' @param endDateTime POSIXct - If not null, the method returns only the production dates which are before this parameter (optional)
#' @param maxItems Integer - Max number of returned production dates (optional, default value NULL). If NULL, returns all the available production dates
#' @param silent Boolean - Active logs if FALSE (optional, default to TRUE)
#' @return Production dates
#' @examples
#' getProductionDates("MF_GRID_CEP_FC_P_3H")
#' getProductionDates("MF_GRID_CEP_FC_P_3H", as.POSIXct("2017-04-13 10:15:30"))
#' getProductionDates("MF_GRID_CEP_FC_P_3H", as.POSIXct("2017-04-13 10:15:30"), as.POSIXct("2017-05-13 10:15:30"))
#' getProductionDates("MF_GRID_CEP_FC_P_3H", as.POSIXct("2017-04-13 10:15:30"), as.POSIXct("2018-02-14 10:15:30"), 5)
#' getProductionDates("MF_GRID_CEP_FC_P_3H", NULL, NULL, 5)
getProductionDates <- function(timeSeriesCode, beginDateTime = NULL, endDateTime = NULL, maxItems = NULL, silent = TRUE) {
  checkLibraries()
  checkGlobalVariables()

  library(dplyr)

  request <- paste(getWimesHost(),
                   "/coredb/api/v2/",
                   get("WIMES_KEY", envir = .GlobalEnv),
                   "/productionDates/",
                   timeSeriesCode,
                   sep = "")

  # optional beginDateTime

  if (!is.null(beginDateTime)) {
    request <- paste(request,
                     "?beginDateTime=",
                     format(beginDateTime, "%Y-%m-%dT%H:%M:%OSZ", tz="UTC"),
                     sep = "")
    nextSeparator <- "&"
  } else {
    nextSeparator <- "?"
  }

  # optional endDateTime

  if (!is.null(endDateTime)) {
    request <- paste(request,
                     nextSeparator,
                     "endDateTime=",
                     format(endDateTime, "%Y-%m-%dT%H:%M:%OSZ", tz="UTC"),
                     sep = "")
  }

  # optional maxItems

  if (is.null(beginDateTime) && is.null(endDateTime)) {
    nextSeparator <- "?"
  } else {
    nextSeparator <- "&"
  }

  if (!is.null(maxItems)) {
    request <- paste(request, nextSeparator, "maxItems=", maxItems, sep="")
  }

  log(request, silent)
  apiResult <- requestAndParseJson(request)

  if (!is.null(apiResult) && !is.null(apiResult$productionDates$productionDateTime)) {
    apiResult$productionDates$productionDateTime <- as.POSIXct(apiResult$productionDates$productionDateTime,
                                                       format = "%Y-%m-%dT%H:%M:%OSZ",
                                                       tz = "UTC")
  }

  if (is.null(apiResult$prev) && is.null(apiResult$'next')) {
    return(apiResult$productionDates)
  } else if (!is.null(apiResult$prev)) {
    return(getProductionDates_bindAndExecutePrev(apiResult$productionDates,
                                                 apiResult$prev,
                                                 silent))
  } else {
    return(getProductionDates_bindAndExecuteNext(apiResult$productionDates,
                                                 apiResult$'next',
                                                 silent))
  }
  return(apiResult)
}

#' Get the productions dates after the date given in parameter
#'
#' @export
#' @param timeSeriesCode String - Time series code
#' @param beginDateTime POSIXct - All the returned production dates are greater or equals to this date
#' @param maxItems Integer - Max number of returned dates (optional, default value 5000)
#' @return Production dates
#' @examples
#' getProductionDates_V1("MF_GRID_CEP_FC_P_3H", as.POSIXct("2017-04-13 10:15:30"))
#' getProductionDates_V1("MF_GRID_CEP_FC_P_3H", as.POSIXct("2017-04-13 10:15:30"), 10)
getProductionDates_V1 <- function(timeSeriesCode, beginDateTime, maxItems = NULL) {
  checkLibraries()
  checkGlobalVariables()

  request <- paste(getWimesHost(),
                   "/coredb/api/v1/",
                   get("WIMES_KEY", envir = .GlobalEnv),
                   "/productionDates/",
                   timeSeriesCode,
                   "/",
                   format(beginDateTime, "%Y-%m-%dT%H:%M:%OSZ", tz="UTC"),
                   sep = "")

  # Optional maxItems

  if (!is.null(maxItems)) {
    request <- paste(request, "?maxItems=", maxItems, sep = '')
  }

  apiResult <- requestAndParseJson(request)

  if (!is.null(apiResult) && !is.null(apiResult$productionDateTime)) {
    apiResult$productionDateTime <- as.POSIXct(apiResult$productionDateTime,
                                               format = "%Y-%m-%dT%H:%M:%OSZ",
                                               tz = "UTC")
  }

  return(apiResult)
}
getProductionDates_bindAndExecutePrev <- function(result, requestStr, silent = TRUE) {
  log(requestStr, silent)
  apiResult <- requestAndParseJson(requestStr)

  apiResult$productionDates$productionDateTime <- as.POSIXct(apiResult$productionDates$productionDateTime,
                                                             format = "%Y-%m-%dT%H:%M:%OSZ",
                                                             tz = "UTC")

  if (is.null(apiResult$prev)) {
    return(bind_rows(result, apiResult$productionDates))
  } else {
    return(getProductionDates_bindAndExecutePrev(bind_rows(result, apiResult$productionDates),
                                                 apiResult$prev,
                                                 silent))
  }
}

getProductionDates_bindAndExecuteNext <- function(result, requestStr, silent = TRUE) {
  log(requestStr, silent)
  apiResult <- requestAndParseJson(requestStr)

  apiResult$productionDates$productionDateTime <- as.POSIXct(apiResult$productionDates$productionDateTime,
                                                             format = "%Y-%m-%dT%H:%M:%OSZ",
                                                             tz = "UTC")

  if (is.null(apiResult$'next')) {
    return(bind_rows(apiResult$productionDates, result))
  } else {
    return(getProductionDates_bindAndExecuteNext(bind_rows(apiResult$productionDates, result),
                                                 apiResult$'next',
                                                 silent))
  }
}


#' Get all forecast values for a given time series and production date
#'
#' @export
#' @param timeSeriesCode String - Time series code (must be a forecast time series)
#' @param productionDateTime POSIXct - Production date time
#' @return Forecast values
#' @examples
#' getForecastValuesByProductionDate("MF_GRID_CEP_FC_P_3H", as.POSIXct("2017-04-13 10:15:30"))
getForecastValuesByProductionDate <- function(timeSeriesCode, productionDateTime) {
  checkLibraries()
  checkGlobalVariables()

  request <- paste(getWimesHost(),
                   "/coredb/api/v1/",
                   get("WIMES_KEY", envir = .GlobalEnv),
                   "/forecastValues/",
                   timeSeriesCode,
                   "/",
                   format(productionDateTime, "%Y-%m-%dT%H:%M:%OSZ", tz="UTC"),
                   sep = "")


  apiResult <- requestAndParseJson(request)

  if (!is.null(apiResult) && !is.null(apiResult$productionDateTime)) {
    apiResult$productionDateTime <- as.POSIXct(apiResult$productionDateTime,
                                               format = "%Y-%m-%dT%H:%M:%OSZ",
                                               tz = "UTC")
  }

  if (!is.null(apiResult) && !is.null(apiResult$forecastDateTime)) {
    apiResult$forecastDateTime <- as.POSIXct(apiResult$forecastDateTime,
                                             format = "%Y-%m-%dT%H:%M:%OSZ",
                                             tz = "UTC")
  }

  return(apiResult)
}


#' Get all forecast values for a given time series, which have a forecast date after the date passed in parameter
#'
#' @export
#' @param timeSeriesCode String - Time series code (must be a forecast time series)
#' @param forecastBeginDateTime POSIXct - All the returned values have a forecast date after or equals to this parameter (optional, default value now())
#' @param maxItems Integer - Max number of returned dates (optional, default value 5000)
#' @return Forecast values
#' @examples
#' getForecastValues("MF_GRID_CEP_FC_P_3H")
#' getForecastValues("MF_GRID_CEP_FC_P_3H", as.POSIXct("2017-04-13 10:15:30"))
#' getForecastValues("MF_GRID_CEP_FC_P_3H", as.POSIXct("2017-04-13 10:15:30"), 6)
getForecastValues <- function(timeSeriesCode, forecastBeginDateTime = NULL, maxItems = NULL) {
  checkLibraries()
  checkGlobalVariables()

  request <- paste(getWimesHost(),
                   "/coredb/api/v1/",
                   get("WIMES_KEY", envir = .GlobalEnv),
                   "/forecastValues/",
                   timeSeriesCode,
                   sep = "")

  # optional forecastBeginDateTimeStr

  if (!is.null(forecastBeginDateTime)) {
    request <- paste(request,
                     "?beginDateTime=",
                     format(forecastBeginDateTime, "%Y-%m-%dT%H:%M:%OSZ", tz="UTC"),
                     sep = "")
    nextSeparator <- "&"
  } else {
    nextSeparator <- "?"
  }

  # optional maxItems

  if (!is.null(maxItems)) {
    request <- paste(request, nextSeparator, "maxItems=", maxItems, sep = "")
  }

  apiResult <- requestAndParseJson(request)

  if (!is.null(apiResult) && !is.null(apiResult$productionDateTime)) {
    apiResult$productionDateTime <- as.POSIXct(apiResult$productionDateTime,
                                               format = "%Y-%m-%dT%H:%M:%OSZ",
                                               tz = "UTC")
  }

  if (!is.null(apiResult) && !is.null(apiResult$forecastDateTime)) {
    apiResult$forecastDateTime <- as.POSIXct(apiResult$forecastDateTime,
                                             format = "%Y-%m-%dT%H:%M:%OSZ",
                                             tz = "UTC")
  }

  return(apiResult)
}

#' Get a forecast value identified by its production and forecast dates
#'
#' @export
#' @param timeSeriesCode String - Time series code (must be a forecast time series)
#' @param productionDateTime POSIXct - Production date time
#' @param forecastDateTime POSIXct - Forecast date time
#' @return Forecast value
#' @examples
#' getForecastValue("MF_GRID_SYMPO_FC_P_3H", as.POSIXct("2018-02-12 23:45:00"), as.POSIXct("2018-02-12 00:00:00"))
getForecastValue <- function(timeSeriesCode, productionDateTime, forecastDateTime) {
  checkLibraries()
  checkGlobalVariables()

  request <- paste(getWimesHost(),
                   "/coredb/api/v1/",
                   get("WIMES_KEY", envir = .GlobalEnv),
                   "/forecastValues/",
                   timeSeriesCode,
                   "/",
                   format(productionDateTime, "%Y-%m-%dT%H:%M:%OSZ", tz="UTC"),
                   "/",
                   format(forecastDateTime, "%Y-%m-%dT%H:%M:%OSZ", tz="UTC"),
                   sep = "")


  apiResult <- requestAndParseJson(request)

  if (!is.null(apiResult) && !is.null(apiResult$productionDateTime)) {
    apiResult$productionDateTime <- as.POSIXct(apiResult$productionDateTime,
                                               format = "%Y-%m-%dT%H:%M:%OSZ",
                                               tz = "UTC")
  }

  if (!is.null(apiResult) && !is.null(apiResult$forecastDateTime)) {
    apiResult$forecastDateTime <- as.POSIXct(apiResult$forecastDateTime,
                                             format = "%Y-%m-%dT%H:%M:%OSZ",
                                             tz = "UTC")
  }

  return(apiResult)
}

